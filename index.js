//S33 - Activity

/**
 * 
 * From instruction 1 - 12 : Fetch
   From instruction 13 - 19: Postman
 */


// fetching all data from todo API
   async function fetchingAllData(){
      const allData = await  fetch(' https://jsonplaceholder.typicode.com/todos',{method: "GET"});
        
      const json = await allData.json();

      const allTitle = await json.map(item => item.title);

      console.log(allTitle);
      
   } 

   fetchingAllData();


   // fetching one item in the to do list
async function fetchSingleData(){


    const oneData = await fetch('https://jsonplaceholder.typicode.com/todos/1',{method: "GET"});

    const json = await oneData.json();

    console.log(json);
}

fetchSingleData();



// POST method for creating new to do item
async function createToDoItem(){
    const item = await fetch('https://jsonplaceholder.typicode.com/todos', {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(
            {
                title: "new item to do",
                completed:false,
                userId:201,
            })
    });

    const json = await item.json();
    console.log(json);

}

createToDoItem();



// PUT method for editing data in the to do item



async function updateTodoItem(){
    const item = await fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method:'PUT',
        headers:{"Content-Type": "application/json"},
        body: JSON.stringify({
                dateCompleted:'Pending',
                description: 'To update item to my to do list data structure.',
                status: 'Pending',
                title: "updated item to do",
                userId:1,
        })
    });

    const json =await item.json();
    console.log(json);
}

updateTodoItem();


// PATCH method updatig to do item for specific field

async function updatedTodoField(){
    const item = await fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PATCH',
        headers: {"Content-Type": 'application/json'},
        body: JSON.stringify(
            {
                completed:false,
                dateCompleted: '07/09/21'
            }
        )
    });

    const json = await item.json();
    console.log(json);
}

updatedTodoField();


// DELETE method to delete one item in the to do list

async function deleteTodoItem(){
    const deletedItem = await fetch('https://jsonplaceholder.typicode.com/todos/1', {method:'DELETE'});

    const json = await deletedItem.json();
    console.log(json);
}

deleteTodoItem();